package multibrandinfotech.developer.gmeagro.ViewModel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.ProductReturnHistoryItemList;
import multibrandinfotech.developer.gmeagro.R;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class ProductReturnHistoryAdapter extends RecyclerView.Adapter<ProductReturnHistoryAdapter.ItemViewHolder> {
    private ArrayList<ProductReturnHistoryItemList> productReturnHistoryItemList;
    private ProductReturnHistoryAdapter.OnItemClickListener rListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(ProductReturnHistoryAdapter.OnItemClickListener listener) {
        rListener = listener;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewSerial;
        public TextView textViewDistributor;
        public TextView textViewReturnDate;

        public ItemViewHolder(@NonNull View itemView, final ProductReturnHistoryAdapter.OnItemClickListener listener) {
            super(itemView);

            textViewSerial = itemView.findViewById(R.id.textView_serial);
            textViewDistributor = itemView.findViewById(R.id.textView_distributor);
            textViewReturnDate = itemView.findViewById(R.id.textView_returnDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ProductReturnHistoryAdapter(ArrayList<ProductReturnHistoryItemList> productReturnHistoryItemList) {
        this.productReturnHistoryItemList = productReturnHistoryItemList;
    }

    @NonNull
    @Override
    public ProductReturnHistoryAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_return_history_list, viewGroup, false);
        ProductReturnHistoryAdapter.ItemViewHolder itemViewHolder = new ProductReturnHistoryAdapter.ItemViewHolder(view, rListener);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductReturnHistoryAdapter.ItemViewHolder itemViewHolder, int i) {
        ProductReturnHistoryItemList currentItem = productReturnHistoryItemList.get(i);

        itemViewHolder.textViewSerial.setText(i + 1 + "");
        itemViewHolder.textViewDistributor.setText(currentItem.getDistributor() + "");
        itemViewHolder.textViewReturnDate.setText(currentItem.getReturnDate() + "");
    }

    @Override
    public int getItemCount() {
        return productReturnHistoryItemList.size();
    }
}
package multibrandinfotech.developer.gmeagro.ViewModel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.SalesOrderHistoryItemList;
import multibrandinfotech.developer.gmeagro.R;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrderHistoryItemAdapter extends RecyclerView.Adapter<SalesOrderHistoryItemAdapter.ItemViewHolder> {

    private ArrayList<SalesOrderHistoryItemList> salesOrderHistoryItemLists;
    private SalesOrderHistoryItemAdapter.OnItemClickListener rListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public SalesOrderHistoryItemAdapter(ArrayList<SalesOrderHistoryItemList> salesOrderHistoryItemLists) {
        this.salesOrderHistoryItemLists = salesOrderHistoryItemLists;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewSerial;
        public TextView textViewItem;
        public TextView textViewQuantity;
        public TextView textViewDeliveryDate;

        public ItemViewHolder(@NonNull View itemView, final SalesOrderHistoryItemAdapter.OnItemClickListener listener) {
            super(itemView);

            textViewSerial = itemView.findViewById(R.id.textView_serial);
            textViewItem = itemView.findViewById(R.id.textView_item);
            textViewQuantity = itemView.findViewById(R.id.textView_quantity);
            textViewDeliveryDate = itemView.findViewById(R.id.textView_delivery_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public void setOnItemClickListener(SalesOrderHistoryItemAdapter.OnItemClickListener listener) {
        rListener = listener;
    }

    @NonNull
    @Override
    public SalesOrderHistoryItemAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sales_order_history_item_list, viewGroup, false);
        SalesOrderHistoryItemAdapter.ItemViewHolder itemViewHolder = new SalesOrderHistoryItemAdapter.ItemViewHolder(view, rListener);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SalesOrderHistoryItemAdapter.ItemViewHolder itemViewHolder, int i) {
        SalesOrderHistoryItemList currentItem = salesOrderHistoryItemLists.get(i);

        itemViewHolder.textViewSerial.setText(i + 1 + "");
        itemViewHolder.textViewItem.setText(currentItem.getItemName() + "");
        itemViewHolder.textViewQuantity.setText(currentItem.getQuantity() + "");
        itemViewHolder.textViewDeliveryDate.setText(currentItem.getDateOfDelivery() + "");
    }

    @Override
    public int getItemCount() {
        return salesOrderHistoryItemLists.size();
    }
}
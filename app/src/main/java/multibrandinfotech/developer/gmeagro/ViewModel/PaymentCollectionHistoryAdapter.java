package multibrandinfotech.developer.gmeagro.ViewModel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.PaymentCollectionHistoryItemList;
import multibrandinfotech.developer.gmeagro.R;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class PaymentCollectionHistoryAdapter extends RecyclerView.Adapter<PaymentCollectionHistoryAdapter.ItemViewHolder> {
    private ArrayList<PaymentCollectionHistoryItemList> paymentCollectionHistoryItemLists;
    private PaymentCollectionHistoryAdapter.OnItemClickListener rListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(PaymentCollectionHistoryAdapter.OnItemClickListener listener) {
        rListener = listener;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewAmount;
        public TextView textViewMoneyReceiptNo;
        public TextView textViewDistributor;
        public TextView textViewReceiveDate;

        public ItemViewHolder(@NonNull View itemView, final PaymentCollectionHistoryAdapter.OnItemClickListener listener) {
            super(itemView);

            textViewAmount = itemView.findViewById(R.id.textView_amount);
            textViewMoneyReceiptNo = itemView.findViewById(R.id.textView_moneyReceiptNo);
            textViewReceiveDate = itemView.findViewById(R.id.textView_receiveDate);
            textViewDistributor = itemView.findViewById(R.id.textView_distributor);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public PaymentCollectionHistoryAdapter(ArrayList<PaymentCollectionHistoryItemList> paymentCollectionHistoryItemLists) {
        this.paymentCollectionHistoryItemLists = paymentCollectionHistoryItemLists;
    }

    @NonNull
    @Override
    public PaymentCollectionHistoryAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_collection_list, viewGroup, false);
        PaymentCollectionHistoryAdapter.ItemViewHolder itemViewHolder = new PaymentCollectionHistoryAdapter.ItemViewHolder(view, rListener);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentCollectionHistoryAdapter.ItemViewHolder itemViewHolder, int i) {
        PaymentCollectionHistoryItemList currentItem = paymentCollectionHistoryItemLists.get(i);

        itemViewHolder.textViewAmount.setText(currentItem.getAmount() + "");
        itemViewHolder.textViewMoneyReceiptNo.setText(currentItem.getMoneyReceiptNo() + "");
        itemViewHolder.textViewReceiveDate.setText(currentItem.getReceiveDate() + "");
        itemViewHolder.textViewDistributor.setText(currentItem.getDistributorId() + "");
    }

    @Override
    public int getItemCount() {
        return paymentCollectionHistoryItemLists.size();
    }
}
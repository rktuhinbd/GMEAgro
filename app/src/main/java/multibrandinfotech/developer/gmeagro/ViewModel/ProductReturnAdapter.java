package multibrandinfotech.developer.gmeagro.ViewModel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.ProductReturnItemList;
import multibrandinfotech.developer.gmeagro.R;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class ProductReturnAdapter extends RecyclerView.Adapter<ProductReturnAdapter.ItemViewHolder> {
    private ArrayList<ProductReturnItemList> productReturnItemList;
    private ProductReturnAdapter.OnItemClickListener rListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(ProductReturnAdapter.OnItemClickListener listener) {
        rListener = listener;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewSerial;
        public TextView textViewItem;
        public TextView textViewQuantity;
        public TextView textViewInvoiceNo;

        public ItemViewHolder(@NonNull View itemView, final ProductReturnAdapter.OnItemClickListener listener) {
            super(itemView);

            textViewSerial = itemView.findViewById(R.id.textView_serial);
            textViewItem = itemView.findViewById(R.id.textView_item);
            textViewQuantity = itemView.findViewById(R.id.textView_quantity);
            textViewInvoiceNo = itemView.findViewById(R.id.textView_invoice_no);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ProductReturnAdapter(ArrayList<ProductReturnItemList> productReturnItemList) {
        this.productReturnItemList = productReturnItemList;
    }

    @NonNull
    @Override
    public ProductReturnAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_return_list, viewGroup, false);
        ProductReturnAdapter.ItemViewHolder itemViewHolder = new ProductReturnAdapter.ItemViewHolder(view, rListener);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductReturnAdapter.ItemViewHolder itemViewHolder, int i) {
        ProductReturnItemList currentItem = productReturnItemList.get(i);

        itemViewHolder.textViewSerial.setText(i + 1 + "");
        itemViewHolder.textViewItem.setText(currentItem.getItem() + "");
        itemViewHolder.textViewQuantity.setText(currentItem.getQuantity() + "");
        itemViewHolder.textViewInvoiceNo.setText(currentItem.getInvoiceNo() + "");
    }

    @Override
    public int getItemCount() {
        return productReturnItemList.size();
    }
}
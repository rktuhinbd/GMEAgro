package multibrandinfotech.developer.gmeagro.ViewModel;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.SalesOrderHistoryResponse;
import multibrandinfotech.developer.gmeagro.R;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrderHistoryAdapter extends RecyclerView.Adapter<SalesOrderHistoryAdapter.ItemViewHolder> {
    private ArrayList<SalesOrderHistoryResponse> salesOrderHistoryResponses;
    private SalesOrderHistoryAdapter.OnItemClickListener rListener;

    public SalesOrderHistoryAdapter(ArrayList<SalesOrderHistoryResponse> salesOrderHistoryResponses) {
        this.salesOrderHistoryResponses = salesOrderHistoryResponses;
    }

    public void setOnItemClickListener(SalesOrderHistoryAdapter.OnItemClickListener listener) {
        rListener = listener;
    }

    @NonNull
    @Override
    public SalesOrderHistoryAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sales_order_history_list, viewGroup, false);
        SalesOrderHistoryAdapter.ItemViewHolder itemViewHolder = new SalesOrderHistoryAdapter.ItemViewHolder(view, rListener);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SalesOrderHistoryAdapter.ItemViewHolder itemViewHolder, int i) {
        SalesOrderHistoryResponse currentItem = salesOrderHistoryResponses.get(i);

        itemViewHolder.textViewSerial.setText(i + 1 + "");
        itemViewHolder.textViewDistributor.setText(currentItem.getDistributor() + "");
        itemViewHolder.textViewOrderDate.setText(currentItem.getDateOfSale() + "");
    }

    @Override
    public int getItemCount() {
        return salesOrderHistoryResponses.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewSerial;
        public TextView textViewDistributor;
        public TextView textViewOrderDate;

        public ItemViewHolder(@NonNull View itemView, final SalesOrderHistoryAdapter.OnItemClickListener listener) {
            super(itemView);

            textViewSerial = itemView.findViewById(R.id.textView_serial);
            textViewDistributor = itemView.findViewById(R.id.textView_distributor);
            textViewOrderDate = itemView.findViewById(R.id.textView_order_date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
package multibrandinfotech.developer.gmeagro.ViewModel;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

import multibrandinfotech.developer.gmeagro.R;

public class cDatePickerFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog, (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);

        return datePickerDialog;
    }
}
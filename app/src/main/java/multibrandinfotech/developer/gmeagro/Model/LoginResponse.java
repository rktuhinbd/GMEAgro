package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/13/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class LoginResponse {
    private String stat, message, name, session_id;

    public LoginResponse(String stat, String message, String name, String session_id) {
        this.stat = stat;
        this.message = message;
        this.name = name;
        this.session_id = session_id;
    }

    public String getStat() {
        return stat;
    }

    public String getMessage() {
        return message;
    }

    public String getName() {
        return name;
    }

    public String getSession_id() {
        return session_id;
    }
}
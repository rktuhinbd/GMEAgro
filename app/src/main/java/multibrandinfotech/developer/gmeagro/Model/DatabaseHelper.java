package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "gmeagro.db";
    private static final int VERSION = 1;
    private static final String SALES_ORDER_TABLE = "SALES_ORDER";
    private static final String PAYMENT_COLLECTION_TABLE = "PAYMENT_COLLECTION";
    private static final String PRODUCT_RETURN_TABLE = "PRODUCT_RETURN";
    private static final String ID = "id";
    private static final String SELLER_ID = "seller_id";
    private static final String SELL_ORDER_NUMBER = "so_number";
    private static final String PAYMENT_TYPE = "payment_type";
    private static final String DISTRIBUTOR_ID = "distributor_id";
    private static final String ORDER_DATE = "order_date";
    private static final String RETURN_DATE = "return_date";
    private static final String COLLECTION_DATE = "collection_date";
    private static final String CHEQUE_DATE = "cheque_date";
    private static final String DELIVERY_DATE = "delivery_date";
    private static final String ITEM_ID = "item_id";
    private static final String QUANTITY = "quantity";
    private static final String UNIT_PRICE = "unit_price";
    private static final String AMOUNT = "amount";
    private static final String BANK_NAME = "bank_name";
    private static final String ACCOUNT_NAME = "account_name";
    private static final String MONEY_RECEIPT_NO = "money_receipt_no";
    private static final String CHEQUE_NO = "cheque_no";
    private static final String DISCOUNT = "discount";
    private static final String INVOICE_NO = "invoice_no";

    private static final String CREATE_SALES_ORDER_TABLE = "CREATE TABLE " + SALES_ORDER_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SELLER_ID + " VARCHAR(100) NOT NULL, "
            + SELL_ORDER_NUMBER + " VARCHAR(100) NOT NULL, "
            + PAYMENT_TYPE + " VARCHAR(10) NOT NULL, "
            + DISTRIBUTOR_ID + " VARCHAR(100) NOT NULL, "
            + ORDER_DATE + " DATE  NOT NULL, "
            + DELIVERY_DATE + " DATE NOT NULL, "
            + ITEM_ID + " VARCHAR(100) NOT NULL, "
            + QUANTITY + " DOUBLE NOT NULL, "
            + UNIT_PRICE + " DOUBLE NOT NULL, "
            + DISCOUNT + " DOUBLE);";

    private static final String CREATE_PAYMENT_COLLECTION_TABLE = "CREATE TABLE " + PAYMENT_COLLECTION_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PAYMENT_TYPE + " VARCHAR(10) NOT NULL, "
            + SELLER_ID + " VARCHAR(100) NOT NULL, "
            + DISTRIBUTOR_ID + " VARCHAR(100) NOT NULL, "
            + AMOUNT + " DOUBLE NOT NULL, "
            + MONEY_RECEIPT_NO + " VARCHAR(100) NOT NULL, "
            + BANK_NAME + " VARCHAR(100) NOT NULL, "
            + ACCOUNT_NAME + " VARCHAR(100) NOT NULL, "
            + CHEQUE_NO + " VARCHAR(100) NOT NULL, "
            + CHEQUE_DATE + " DATE NOT NULL, "
            + COLLECTION_DATE + " DATE  NOT NULL);";

    private static final String CREATE_PRODUCT_RETURN_TABLE = "CREATE TABLE " + PRODUCT_RETURN_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SELLER_ID + " VARCHAR(100) NOT NULL, "
            + DISTRIBUTOR_ID + " VARCHAR(100) NOT NULL, "
            + ITEM_ID + " VARCHAR(100) NOT NULL, "
            + QUANTITY + " DOUBLE NOT NULL, "
            + INVOICE_NO + " VARCHAR(100) NOT NULL, "
            + RETURN_DATE + " DATE  NOT NULL);";

    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_SALES_ORDER_TABLE);
            db.execSQL(CREATE_PAYMENT_COLLECTION_TABLE);
            db.execSQL(CREATE_PRODUCT_RETURN_TABLE);
        } catch (Exception e) {
            Toast.makeText(context, "Exception: " + e, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SALES_ORDER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PAYMENT_COLLECTION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PRODUCT_RETURN_TABLE);
        onCreate(db);
    }

    public Cursor fetchSalesOrder() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("Select * from " + SALES_ORDER_TABLE, null);
        return cursor;
    }

    public long insertSalesOrder(String sellerId, String soNo, String paymentType, String distributorId, String orderDate, String deliveryDate, String itemId, String quantity, String unitPrice, String discount) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SELLER_ID, sellerId);
        contentValues.put(SELL_ORDER_NUMBER, soNo);
        contentValues.put(PAYMENT_TYPE, paymentType);
        contentValues.put(DISTRIBUTOR_ID, distributorId);
        contentValues.put(ORDER_DATE, orderDate);
        contentValues.put(DELIVERY_DATE, deliveryDate);
        contentValues.put(ITEM_ID, itemId);
        contentValues.put(QUANTITY, quantity);
        contentValues.put(UNIT_PRICE, unitPrice);
        contentValues.put(DISCOUNT, discount);

        long input = sqLiteDatabase.insert(SALES_ORDER_TABLE, null, contentValues);
        return input;
    }

    public long insertPaymentReturn(String paymentType, String sellerId, String distributorId, String amount, String moneyReceiptNo, String bankName, String accountName, String chequeNo, String chequeDate, String collectionDate) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PAYMENT_TYPE, paymentType);
        contentValues.put(SELLER_ID, sellerId);
        contentValues.put(DISTRIBUTOR_ID, distributorId);
        contentValues.put(AMOUNT, amount);
        contentValues.put(MONEY_RECEIPT_NO, moneyReceiptNo);
        contentValues.put(BANK_NAME, bankName);
        contentValues.put(ACCOUNT_NAME, accountName);
        contentValues.put(CHEQUE_NO, chequeNo);
        contentValues.put(CHEQUE_DATE, chequeDate);
        contentValues.put(COLLECTION_DATE, collectionDate);

        long input = sqLiteDatabase.insert(PAYMENT_COLLECTION_TABLE, null, contentValues);
        return input;
    }

    public long insertProductReturn(String sellerId, String distributorId, String itemId, String quantity, String invoiceNo, String returnDate) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SELLER_ID, sellerId);
        contentValues.put(DISTRIBUTOR_ID, distributorId);
        contentValues.put(ITEM_ID, itemId);
        contentValues.put(QUANTITY, quantity);
        contentValues.put(INVOICE_NO, invoiceNo);
        contentValues.put(RETURN_DATE, returnDate);

        long input = sqLiteDatabase.insert(PRODUCT_RETURN_TABLE, null, contentValues);
        return input;
    }

    public void clearSalesOrder() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM " + SALES_ORDER_TABLE);
    }
}
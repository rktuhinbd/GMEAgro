package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 2/4/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrderHistoryItemList {
    private String itemName, quantity, dateOfDelivery;

    public SalesOrderHistoryItemList(String itemName, String quantity, String dateOfDelivery) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.dateOfDelivery = dateOfDelivery;
    }

    public String getItemName() {
        return itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getDateOfDelivery() {
        return dateOfDelivery;
    }
}
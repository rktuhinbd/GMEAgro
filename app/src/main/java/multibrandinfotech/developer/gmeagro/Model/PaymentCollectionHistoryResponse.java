package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/28/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

public class PaymentCollectionHistoryResponse {
    private String distributorId, amount, moneyReceiptNo, receiveDate;

    public PaymentCollectionHistoryResponse(String distributorId, String amount, String moneyReceiptNo, String receiveDate) {
        this.distributorId = distributorId;
        this.amount = amount;
        this.moneyReceiptNo = moneyReceiptNo;
        this.receiveDate = receiveDate;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public String getAmount() {
        return amount;
    }

    public String getMoneyReceiptNo() {
        return moneyReceiptNo;
    }

    public String getReceiveDate() {
        return receiveDate;
    }
}
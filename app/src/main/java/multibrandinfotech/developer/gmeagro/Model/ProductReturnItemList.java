package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

public class ProductReturnItemList {
    private String item;
    private String  quantity;
    private String invoiceNo;

    public ProductReturnItemList(String item, String  quantity, String invoiceNo) {
        this.item = item;
        this.quantity = quantity;
        this.invoiceNo = invoiceNo;
    }

    public String getItem() {
        return item;
    }

    public String  getQuantity() {
        return quantity;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }
}
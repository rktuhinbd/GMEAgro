package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/22/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class TopDueResponse {
    private String name;
    private int amount;

    public TopDueResponse(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }
}
package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/22/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SellerActivityResponse {
    private int target, achievement;

    public SellerActivityResponse(int target, int achievement) {
        this.target = target;
        this.achievement = achievement;
    }

    public int getTarget() {
        return target;
    }

    public int getAchievement() {
        return achievement;
    }
}
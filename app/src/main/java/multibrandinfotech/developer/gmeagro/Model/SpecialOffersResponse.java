package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/19/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SpecialOffersResponse {
    private String offers;

    public SpecialOffersResponse(String offers) {
        this.offers = offers;
    }

    public String getOffers() {
        return offers;
    }
}
package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/31/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

public class ProductReturnHistoryResponse {

    private String distributorId, returnDate;

    public ProductReturnHistoryResponse(String distributorId, String returnDate) {
        this.distributorId = distributorId;

        this.returnDate = returnDate;
    }

    public String getDistributor() {
        return distributorId;
    }

    public String getReturnDate() {
        return returnDate;
    }
}
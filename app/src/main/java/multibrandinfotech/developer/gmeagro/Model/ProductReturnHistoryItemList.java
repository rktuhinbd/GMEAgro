package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

public class ProductReturnHistoryItemList {
    private String distributor, returnDate;

    public ProductReturnHistoryItemList(String distributor, String returnDate) {
        this.distributor = distributor;

        this.returnDate = returnDate;
    }

    public String getDistributor() {
        return distributor;
    }

    public String getReturnDate() {
        return returnDate;
    }
}
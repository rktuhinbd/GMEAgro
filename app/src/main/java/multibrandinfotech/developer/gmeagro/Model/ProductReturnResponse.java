package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/17/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class ProductReturnResponse {
    private String response;

    public ProductReturnResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
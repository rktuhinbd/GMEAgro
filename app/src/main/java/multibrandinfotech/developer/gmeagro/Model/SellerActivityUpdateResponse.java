package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/24/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SellerActivityUpdateResponse {
    private int achievement;

    public SellerActivityUpdateResponse(int achievement) {
        this.achievement = achievement;
    }

    public int getAchievement() {
        return achievement;
    }
}
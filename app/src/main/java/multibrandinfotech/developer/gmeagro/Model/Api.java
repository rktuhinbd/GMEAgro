package multibrandinfotech.developer.gmeagro.Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Md. Rejaul Karim on 1/5/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public interface Api {

    //Seller Activity
    @GET("seller_activity_data.php")
    Call<List<SellerActivityResponse>> getSellerActivity();

    //Distributors who are in top due list
    @GET("top_due.php")
    Call<List<TopDueResponse>> getTopDue();

    //Special Offers
    @GET("special_offers_data.php")
    Call<List<SpecialOffersResponse>> getOffers();

    //Login
    @FormUrlEncoded
    @POST("http://182.163.102.201:8086/gme_agro/gme_agro/site/appLogin")
    Call<LoginResponse> loginData(
            @Field("user_name") String userName,
            @Field("user_pass") String password
    );

    //Seller Activity updater
    @FormUrlEncoded
    @POST("seller_activity_update.php")
    Call<SellerActivityUpdateResponse> sellerActivityUpdate(
            @Field("achievement") String achievement
    );

    //Sales Order History
    @FormUrlEncoded
    @POST("salesOrderHistory.php")
    Call<List<SalesOrderHistoryResponse>> getSalesOrderHistory(
            @Field("seller_id") String sellerId
    );

    //Sales Order History Items
    @FormUrlEncoded
    @POST("salesOrderHistoryItems.php")
    Call<List<SalesOrderHistoryItemList>> getSalesOrderHistoryItems(
            @Field("distributor_id") String distributorId
    );

    //Product Return History
    @FormUrlEncoded
    @POST("productReturnHistory.php")
    Call<List<ProductReturnHistoryResponse>> getProductReturnHistory(
            @Field("seller_id") String sellerId
    );

    //Product Return Items
    @FormUrlEncoded
    @POST("returnedProductList.php")
    Call<List<ReturnedProductResponse>> getReturnedProducts(
            @Field("distributor_id") String distributorId
    );

    //Payment Collection History
    @FormUrlEncoded
    @POST("paymentCollectionHistory.php")
    Call<List<PaymentCollectionHistoryResponse>> getPaymentCollectionHistory(
            @Field("seller_id") String sellerId
    );

    //Sales Order
    @FormUrlEncoded
    @POST("salesOrder.php")
    Call<SalesOrderResponse> salesOrder(
            @Field("seller_id") String sellerId,
            @Field("so_number") String soNo,
            @Field("payment_type") String paymentType,
            @Field("distributor_id") String distributorId,
            @Field("date_of_sale") String dateOfSale,
            @Field("date_of_delivery") String dateOfDelivery,
            @Field("item_id") String itemId,
            @Field("quantity") String quantity,
            @Field("unit_price") String unitPrice,
            @Field("discount") String discount
    );

    //Payment Collection
    @FormUrlEncoded
    @POST("paymentCollection.php")
    Call<PaymentCollectionResponse> paymentCollection(
            @Field("payment_type") String paymentType,
            @Field("seller_id") String sellerId,
            @Field("distributor_id") String distributorId,
            @Field("amount") String amount,
            @Field("money_receipt_no") String moneyReceiptNo,
            @Field("bank_name") String bankName,
            @Field("account_name") String accountName,
            @Field("cheque_no") String chequeNo,
            @Field("cheque_date") String chequeDate,
            @Field("receive_date") String returnDate
    );

    //Product Return
    @FormUrlEncoded
    @POST("productReturn.php")
    Call<ProductReturnResponse> productReturn(
            @Field("seller_id") String sellerId,
            @Field("distributor_id") String distributorId,
            @Field("item_id") String itemId,
            @Field("quantity") String quantity,
            @Field("invoice_no") String invoiceNo,
            @Field("return_date") String returnDate
    );
}
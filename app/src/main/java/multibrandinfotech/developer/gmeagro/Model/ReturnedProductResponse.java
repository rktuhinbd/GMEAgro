package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 2/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class ReturnedProductResponse {
    private String itemName, quantity, invoiceNo, returnDate;

    public ReturnedProductResponse(String itemName, String quantity, String invoiceNo, String returnDate) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.invoiceNo = invoiceNo;
        this.returnDate = returnDate;
    }

    public String getItemName() {
        return itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public String getReturnDate() {
        return returnDate;
    }
}
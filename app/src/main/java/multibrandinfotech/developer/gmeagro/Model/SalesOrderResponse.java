package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/19/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrderResponse {
    private String response;

    public SalesOrderResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
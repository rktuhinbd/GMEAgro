package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 1/28/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

public class PaymentCollectionHistoryItemList {
    private String amount, moneyReceiptNo, receiveDate, distributorId;

    public PaymentCollectionHistoryItemList(String amount, String moneyReceiptNo, String receiveDate, String distributorId) {
        this.amount = amount;
        this.moneyReceiptNo = moneyReceiptNo;
        this.receiveDate = receiveDate;
        this.distributorId = distributorId;
    }

    public String getAmount() {
        return amount;
    }

    public String getMoneyReceiptNo() {
        return moneyReceiptNo;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public String getDistributorId() {
        return distributorId;
    }
}
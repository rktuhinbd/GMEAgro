package multibrandinfotech.developer.gmeagro.Model;

/**
 * Created by Md. Rejaul Karim on 2/4/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrderHistoryResponse {
    private String distributorId, dateOfSale;

    public SalesOrderHistoryResponse(String distributorId, String orderDate) {
        this.distributorId = distributorId;
        this.dateOfSale = orderDate;
    }

    public String getDistributor() {
        return distributorId;
    }

    public String getDateOfSale() {
        return dateOfSale;
    }
}
package multibrandinfotech.developer.gmeagro.View;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.DatabaseHelper;
import multibrandinfotech.developer.gmeagro.Model.SalesOrderItemList;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.SalesOrderAdapter;

public class SalesInvoice extends AppCompatActivity {

    private static DatabaseHelper databaseHelper;
    ArrayList<SalesOrderItemList> items = new ArrayList<>();
    private TextView subTotal, discount, grandTotal, tvDistributor, tvPaymentType, tvInvoiceDate, tvDeliveryDate;
    private Button button;
    private int total = 0, dis = 0, grand_total = 0;
    private String sellerId, paymentType, distributorId, dateOfSale, dateOfDelivery, itemId, quantity, unitPrice, discountText, subTotalText, grandTotalText;
    private String[] sellerIds, paymentTypes, distributorIds, dateOfSales, dateOfDeliveries, itemIds, quantities, unitPrices, discounts;
    private RecyclerView recyclerView;
    private SalesOrderAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_invoice);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sales Invoice");

        subTotal = (TextView) findViewById(R.id.textView_sub_total);
        discount = (TextView) findViewById(R.id.textView_discount);
        grandTotal = (TextView) findViewById(R.id.textView_grand_total);
        tvDistributor = (TextView) findViewById(R.id.tvDistributorName);
        tvPaymentType = (TextView) findViewById(R.id.tvPaymentType);
        tvInvoiceDate = (TextView) findViewById(R.id.tvInvoiceDate);
        tvDeliveryDate = (TextView) findViewById(R.id.tvDeliveryDate);

        button = (Button) findViewById(R.id.ok);

        databaseHelper = new DatabaseHelper(this);

        //Fetch Database Values
        Cursor cursor = databaseHelper.fetchSalesOrder();

        //Array size assignment
        sellerIds = new String[cursor.getCount()];
        paymentTypes = new String[cursor.getCount()];
        distributorIds = new String[cursor.getCount()];
        dateOfSales = new String[cursor.getCount()];
        dateOfDeliveries = new String[cursor.getCount()];
        itemIds = new String[cursor.getCount()];
        quantities = new String[cursor.getCount()];
        unitPrices = new String[cursor.getCount()];
        discounts = new String[cursor.getCount()];

        int i = 0;
        if (cursor.getCount() == 0) {
            Toast.makeText(getApplication(), "Database is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        while (cursor.moveToNext()) {
            sellerId = cursor.getString(1);
            paymentType = cursor.getString(3);
            distributorId = cursor.getString(4);
            dateOfSale = cursor.getString(5);
            dateOfDelivery = cursor.getString(6);
            itemId = cursor.getString(7);
            quantity = cursor.getString(8);
            unitPrice = cursor.getString(9);
            discountText = cursor.getString(10);

            sellerIds[i] = sellerId;
            paymentTypes[i] = paymentType;
            distributorIds[i] = distributorId;
            dateOfSales[i] = dateOfSale;
            dateOfDeliveries[i] = dateOfDelivery;
            itemIds[i] = itemId;
            quantities[i] = quantity;
            unitPrices[i] = unitPrice;
            discounts[i] = discountText;

            int discountInt = Integer.parseInt(discountText.replaceAll("\\D+", ""));
            double price = Double.parseDouble(unitPrice);
            double amount = (price - discountInt) * Double.parseDouble(quantity);
            String discountString = discountInt + " %";

            total += amount;
            dis += discountInt;

            items.add(new SalesOrderItemList(itemId, Double.parseDouble(quantity), Integer.parseInt(unitPrice), discountString, amount));
            populateRecyclerView();

            i++;
        }

        grand_total = total - dis;

        tvDistributor.setText(distributorId);
        tvPaymentType.setText(paymentType);
        tvInvoiceDate.setText(dateOfSale);
        tvDeliveryDate.setText(dateOfDelivery);
        subTotal.setText(String.valueOf(total));
        discount.setText(String.valueOf(dis));
        grandTotal.setText(String.valueOf(grand_total));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.clearSalesOrder();
                Intent intent = new Intent(getApplicationContext(), IndentForm.class);
                intent.putExtra("user", sellerId);
                startActivity(intent);
            }
        });
    }

    public void populateRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(this);
        adapter = null;
        adapter = new SalesOrderAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
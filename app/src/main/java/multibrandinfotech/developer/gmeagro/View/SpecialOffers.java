package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.Api;
import multibrandinfotech.developer.gmeagro.Model.SpecialOffersResponse;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SpecialOffers extends AppCompatActivity {

    private InternetConnectionChecker connectionChecker;
    private ListView listViewSpecialOffers;
    private String[] specialOffers;
    private String sellerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_offers);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Special Offers");

        connectionChecker = new InternetConnectionChecker(this);
        sellerId = getIntent().getStringExtra("user");
        listViewSpecialOffers = (ListView) findViewById(R.id.specialOffers);

        if (connectionChecker.isConnected()) {
            getSpecialOffers();
        } else {
            Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_special_offers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menuDashboard:
                Intent i = new Intent(getApplicationContext(), Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuPaymentCollection:
                Intent i1 = new Intent(getApplicationContext(), PaymentCollection.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuProductReturn:
                Intent i2 = new Intent(getApplicationContext(), ProductReturn.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;

            case R.id.menuSellItems:
                Intent intent = new Intent(getApplicationContext(), IndentForm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("user", sellerId);
                startActivity(intent);
                break;

            case R.id.menuReports:
                Intent iR = new Intent(getApplicationContext(), Reports.class);
                iR.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iR.putExtra("user", sellerId);
                startActivity(iR);
                break;

            case R.id.menuSignOut:
                Intent i3 = new Intent(getApplicationContext(), Login.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.menuAbout:
                Intent i4 = new Intent(getApplicationContext(), About.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    public void getSpecialOffers() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://182.163.102.201:8086/android_test/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<SpecialOffersResponse>> call = api.getOffers();
        call.enqueue(new Callback<List<SpecialOffersResponse>>() {
            @Override
            public void onResponse(Call<List<SpecialOffersResponse>> call, Response<List<SpecialOffersResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Response Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    Log.e("Special Offers", "onResponse: " + response.code() + "\n\n");
                    return;
                } else {
                    List<SpecialOffersResponse> offersResponses = response.body();

                    specialOffers = new String[offersResponses.size()];

                    for (int i = 0; i < offersResponses.size(); i++) {
                        specialOffers[i] = offersResponses.get(i).getOffers();
                    }
                    listViewSpecialOffers.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, specialOffers));
                }
            }

            @Override
            public void onFailure(Call<List<SpecialOffersResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailureResponse: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Special Offers", "onFailureResponse: " + t.getMessage() + "\n\n");
            }
        });
    }
}
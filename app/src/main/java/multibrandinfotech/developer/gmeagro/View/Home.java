package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.Api;
import multibrandinfotech.developer.gmeagro.Model.SellerActivityResponse;
import multibrandinfotech.developer.gmeagro.Model.TopDueResponse;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.graphics.Color.rgb;

public class Home extends AppCompatActivity {

    private InternetConnectionChecker connectionChecker;
    private long backPressTime;
    private Toast backToast;

    private BarChart barChart;
    private BarDataSet barDataSet;

    private ArrayList<BarEntry> barEntries = new ArrayList<>();
    private ArrayList<String> barLabels = new ArrayList<String>();
    private ArrayList<Integer> barColors = new ArrayList<Integer>();

    private ListView listViewTopDue;
    private String[] topDue;
    private String user;
    private int target = 100, achievement = 70;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        connectionChecker = new InternetConnectionChecker(this);
        listViewTopDue = (ListView) findViewById(R.id.topDue);
        user = getIntent().getStringExtra("user");

        if (connectionChecker.isConnected()) {
            getSellerActivity();
            createTopDueList();
        } else {
            Toast.makeText(getApplicationContext(), "Check your internet connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void createBarChart() {
        barChart = (BarChart) findViewById(R.id.BarChart);

        barLabels.add("Target");
        barLabels.add("Achieved");

        barColors.add(rgb(230, 126, 34));
        barColors.add(rgb(46, 204, 113));

        barEntries.add(new BarEntry(0, target));
        barEntries.add(new BarEntry(1, achievement));

        XAxis xAxis = barChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(barLabels));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);
        xAxis.setDrawGridLines(false);

        YAxis yr = barChart.getAxisRight();
        yr.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        yr.setDrawGridLines(false);
        yr.setEnabled(false);
        yr.setAxisMinimum(0f);

        barDataSet = new BarDataSet(barEntries, "Sales Analysis");
        barDataSet.setColors(barColors);

        BarData data = new BarData(barDataSet);
        data.setBarWidth(0.4f);

        barChart.setDrawBarShadow(false);
        barChart.getLegend().setEnabled(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setMaxVisibleValueCount(100);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);
        barChart.setTouchEnabled(false);
        barChart.setDragEnabled(false);
        barChart.setScaleEnabled(true);
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisRight().setAxisMinimum(0);
        barChart.setDescription(null);
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(barLabels));
        barChart.setData(data);
        barChart.animateY(1000);
        barChart.invalidate();
    }

    public void getSellerActivity() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://182.163.102.201:8086/android_test/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<SellerActivityResponse>> call = api.getSellerActivity();
        call.enqueue(new Callback<List<SellerActivityResponse>>() {
            @Override
            public void onResponse(Call<List<SellerActivityResponse>> call, Response<List<SellerActivityResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Response Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    Log.e("Bar Chart", "onResponse: " + response.code());
                    return;
                } else {
                    List<SellerActivityResponse> responses = response.body();

                    for (int i = 0; i < responses.size(); i++) {
                        target = responses.get(i).getTarget();
                        achievement = responses.get(i).getAchievement();
                    }
                    createBarChart();
                }
            }

            @Override
            public void onFailure(Call<List<SellerActivityResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailureResponse: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Bar Chart", "onFailureResponse: " + t.getMessage());
            }
        });
    }

    public void createTopDueList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://182.163.102.201:8086/android_test/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<TopDueResponse>> call = api.getTopDue();
        call.enqueue(new Callback<List<TopDueResponse>>() {
            @Override
            public void onResponse(Call<List<TopDueResponse>> call, Response<List<TopDueResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Response Failure: " + response.code(), Toast.LENGTH_LONG).show();
                    Log.e("Top Due", "onResponse: " + response.code());
                    return;
                } else {
                    List<TopDueResponse> responses = response.body();

                    topDue = new String[responses.size()];

                    for (int i = 0; i < responses.size(); i++) {
                        topDue[i] = responses.get(i).getName() + " - " + responses.get(i).getAmount();
                    }
                    listViewTopDue.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, topDue));
                }
            }

            @Override
            public void onFailure(Call<List<TopDueResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "onFailureResponse: " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Top Due", "onFailureResponse: " + t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSellItems:
                Intent intent = new Intent(getApplicationContext(), IndentForm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("user", user);
                startActivity(intent);
                break;

            case R.id.menuPaymentCollection:
                Intent i = new Intent(getApplicationContext(), PaymentCollection.class);
                i.putExtra("user", user);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;

            case R.id.menuProductReturn:
                Intent i1 = new Intent(getApplicationContext(), ProductReturn.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", user);
                startActivity(i1);
                break;

            case R.id.menuSpeicalOffers:
                Intent i2 = new Intent(getApplicationContext(), SpecialOffers.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", user);
                startActivity(i2);
                break;

            case R.id.menuReports:
                Intent iR = new Intent(getApplicationContext(), Reports.class);
                iR.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iR.putExtra("user", user);
                startActivity(iR);
                break;

            case R.id.menuSignOut:
                Intent i3 = new Intent(getApplicationContext(), Login.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.menuAbout:
                Intent i4 = new Intent(getApplicationContext(), About.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        if (backPressTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        } else {
            backToast = Toast.makeText(getApplicationContext(), "Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressTime = System.currentTimeMillis();
    }
}
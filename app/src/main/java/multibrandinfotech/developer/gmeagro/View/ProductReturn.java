package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import multibrandinfotech.developer.gmeagro.Model.DatabaseHelper;
import multibrandinfotech.developer.gmeagro.Model.ProductReturnItemList;
import multibrandinfotech.developer.gmeagro.Model.ProductReturnResponse;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.ProductReturnAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductReturn extends AppCompatActivity {

    private static DatabaseHelper databaseHelper;
    private InternetConnectionChecker connectionChecker;
    private AutoCompleteTextView editTextDistributor, editTextItemName;
    private EditText editTextQuantity, editTextInvoiceNo;
    private Button buttonAddItem, buttonReturn;

    private RecyclerView recyclerView;
    private ProductReturnAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<ProductReturnItemList> items = new ArrayList<>();
    private String[] distributorArray, Products;

    private String sellerId, distributor, itemName, quantity, invoiceNo, currentDate;
    private int Position, distributorFlag = 0, itemNameFlag = 0, quantityFlag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_return);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product Return");

        sellerId = getIntent().getStringExtra("user");

        connectionChecker = new InternetConnectionChecker(this);
        databaseHelper = new DatabaseHelper(this);

        editTextDistributor = (AutoCompleteTextView) findViewById(R.id.et_Distributor);
        editTextItemName = (AutoCompleteTextView) findViewById(R.id.et_ItemName);
        editTextQuantity = (EditText) findViewById(R.id.et_Quantity);
        editTextInvoiceNo = (EditText) findViewById(R.id.et_InvoiceNo);
        buttonAddItem = (Button) findViewById(R.id.btn_AddItem);
        buttonReturn = (Button) findViewById(R.id.btn_Return);

        distributorArray = getResources().getStringArray(R.array.distributorName);
        Products = getResources().getStringArray(R.array.productName);

        final ArrayAdapter<String> itemNameAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Products);
        final ArrayAdapter<String> distributorAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, distributorArray);

        editTextItemName.setAdapter(itemNameAdapter);
        editTextDistributor.setAdapter(distributorAdapter);

        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView = findViewById(R.id.recyclerView);
                adapter = new ProductReturnAdapter(items);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);

                if (adapter.getItemCount() > 0) {
                    openProductReturnDialog();
                } else {
                    Toast.makeText(getApplicationContext(), "Please add some items to proceed product return!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getRecyclerViewItemPosition(int position) {
        ProductReturnItemList itemPosition = items.get(position);
        adapter.notifyItemChanged(position);
        Position = position;
    }

    public void populateRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(this);
        adapter = null;
        adapter = new ProductReturnAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ProductReturnAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                getRecyclerViewItemPosition(position);
                openRecyclerViewItemDeleteDialog();
            }
        });
    }

    public void addItem() {
        validateDistributor();
        validateItemName();
        validateQuantity();

        if (distributorFlag == 1 && itemNameFlag == 1 && quantityFlag == 1) {
            distributorFlag = 0;
            itemNameFlag = 0;
            quantityFlag = 0;

            itemName = editTextItemName.getText().toString();
            quantity = editTextQuantity.getText().toString();
            invoiceNo = editTextInvoiceNo.getText().toString();
            items.add(new ProductReturnItemList(itemName, quantity, invoiceNo));
            populateRecyclerView();

            clearInputs();
        } else {
            Toast.makeText(getApplicationContext(), "Do not leave any input blank", Toast.LENGTH_SHORT).show();
        }
    }

    public void removeItem(int position) {
        items.remove(position);
        adapter.notifyItemRemoved(position);

        populateRecyclerView();

        Toast.makeText(ProductReturn.this, "Item removed", Toast.LENGTH_SHORT).show();
    }

    public void openRecyclerViewItemDeleteDialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Remove Item");
        adb.setMessage("Do you want to remove this item?");
        adb.setIcon(R.drawable.ic_remove_item);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                removeItem(Position);
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adb.show();
    }

    private void clearInputs() {
        editTextItemName.setText("");
        editTextQuantity.setText("");
        editTextInvoiceNo.setText("");
    }

    private void removeErrors() {
        editTextDistributor.setError(null);
        editTextItemName.setError(null);
        editTextQuantity.setError(null);
        editTextInvoiceNo.setError(null);
    }

    public void validateItemName() {
        if (editTextItemName.getText().toString().equals("")) {
            editTextItemName.setError("Item name is required!");
        } else {
            editTextItemName.setError(null);
            itemNameFlag = 1;
        }
    }

    public void validateDistributor() {
        if (editTextDistributor.getText().toString().equals("")) {
            editTextDistributor.setError("Distributor is required!");
        } else {
            distributorFlag = 1;
            editTextDistributor.setError(null);
        }
    }

    public void validateQuantity() {
        if (editTextQuantity.getText().toString().equals("")) {
            editTextQuantity.setError("Quantity is required!");
        } else {
            editTextQuantity.setError(null);
            quantityFlag = 1;
        }
    }

    public void postJSONData() {
        distributor = editTextDistributor.getText().toString();
        for (int i = 0; i < items.size(); i++) {
            Call<ProductReturnResponse> call = new RetrofitClient()
                    .getApi()
                    .productReturn(sellerId, distributor, items.get(i).getItem(), String.valueOf(items.get(i).getQuantity()), items.get(i).getInvoiceNo(), currentDate);

            call.enqueue(new Callback<ProductReturnResponse>() {
                @Override
                public void onResponse(Call<ProductReturnResponse> call, Response<ProductReturnResponse> response) {
                    ProductReturnResponse returnResponse = response.body();
                    //Toast.makeText(getApplicationContext(), "Product return request successfully submitted!!!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ProductReturnResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Error!!! Product return unsuccessful", Toast.LENGTH_LONG).show();
                    Log.e("Product Return Request!", "onFailure: " + t.getMessage());
                }
            });
        }
    }

    public void productReturn() {
        //Get current date
        Date date = new Date();
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date.getTime());

        if (connectionChecker.isConnected()) {
            postJSONData();
            clearInputs();
            removeErrors();
        } else {
            for (int i = 0; i < items.size(); i++) {
                databaseHelper.insertProductReturn(sellerId, distributor, items.get(i).getItem(), items.get(i).getQuantity(), items.get(i).getInvoiceNo(), currentDate);
            }
            Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_SHORT).show();
        }
        editTextDistributor.setText("");
        clearInputs();
        removeErrors();
        items.clear();
        populateRecyclerView();
    }

    public void openProductReturnDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Product Return");
        adb.setMessage("Do you want to submit Product Return request?");
        adb.setIcon(R.drawable.ic_confirm);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                productReturn();
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adb.show();
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_product_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDashboard:
                Intent i1 = new Intent(getApplicationContext(), Home.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuSellItems:
                Intent intent = new Intent(getApplicationContext(), IndentForm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("user", sellerId);
                startActivity(intent);
                break;

            case R.id.menuPaymentCollection:
                Intent i = new Intent(getApplicationContext(), PaymentCollection.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuSpeicalOffers:
                Intent i2 = new Intent(getApplicationContext(), SpecialOffers.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;

            case R.id.menuReports:
                Intent iR = new Intent(getApplicationContext(), Reports.class);
                iR.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iR.putExtra("user", sellerId);
                startActivity(iR);
                break;

            case R.id.menuSignOut:
                Intent i3 = new Intent(getApplicationContext(), Login.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.menuAbout:
                Intent i4 = new Intent(getApplicationContext(), About.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("user", sellerId);
        startActivity(intent);
    }
}
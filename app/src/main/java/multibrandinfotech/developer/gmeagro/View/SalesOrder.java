package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import multibrandinfotech.developer.gmeagro.Model.DatabaseHelper;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.Model.SalesOrderItemList;
import multibrandinfotech.developer.gmeagro.Model.SalesOrderResponse;
import multibrandinfotech.developer.gmeagro.Model.SellerActivityUpdateResponse;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.SalesOrderAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesOrder extends AppCompatActivity {

    private static DatabaseHelper databaseHelper;
    private ArrayList<SalesOrderItemList> items = new ArrayList<>();
    private ArrayAdapter<String> itemNameAdapter, itemCodeAdapter;
    private InternetConnectionChecker connectionChecker;
    private EditText editTextQuantity;
    private AutoCompleteTextView editTextItemName, editTextItemCode;
    private TextView textVIewTotalAmount;
    private Button buttonAddItem, buttonPlaceOrder;
    private String sellerId, paymentType, distributorId, dateOfSale, dateOfDelivery, itemName, itemCode, quantityString, discountString;
    private int quantityInt = 0, price, percent = 0, grandTotal, Position, soNo, i, t;
    private double discountDouble = 1, amount, totalAmount = 0, subtractTotal;

    //private NotificationManagerCompat notificationManager;
    private RecyclerView recyclerView;
    private SalesOrderAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private long backPressTime;
    private Toast backToast;
    private String[] UnitPrice, Products;
    private ArrayList<String> itemNames = new ArrayList<>();

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            for (int i = 0; i < Products.length; i++) {
                if (s.toString().equals(Products[i])) {
                    //Set Text in itemCode - AutoComplete TextView
                    editTextItemCode.setText(s);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        connectionChecker = new InternetConnectionChecker(this);
        //notificationManager = NotificationManagerCompat.from(getBaseContext());

        editTextItemName = (AutoCompleteTextView) findViewById(R.id.editText_ItemName);
        editTextItemCode = (AutoCompleteTextView) findViewById(R.id.editText_ItemCode);
        editTextQuantity = (EditText) findViewById(R.id.editText_Quantity);
        textVIewTotalAmount = (TextView) findViewById(R.id.textView_TotalAmount);

        buttonAddItem = (Button) findViewById(R.id.button_AddItem);
        buttonPlaceOrder = (Button) findViewById(R.id.button_PlaceOrder);

        databaseHelper = new DatabaseHelper(this);

        final String Flag = getIntent().getStringExtra("flag");
        sellerId = getIntent().getStringExtra("seller_id");
        paymentType = getIntent().getStringExtra("payment_type");
        distributorId = getIntent().getStringExtra("distributor_id");
        dateOfSale = getIntent().getStringExtra("date_of_sale");
        dateOfDelivery = getIntent().getStringExtra("date_of_delivery");

        Products = getResources().getStringArray(R.array.productName);
        itemNameAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Products);
        itemCodeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Products);

        editTextItemName.setAdapter(itemNameAdapter);
        editTextItemCode.setAdapter(itemCodeAdapter);

        editTextItemName.addTextChangedListener(watcher);

        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantityString = editTextQuantity.getText().toString();
                itemName = editTextItemName.getText().toString();
                itemCode = editTextItemCode.getText().toString();

                if (Flag.equals("1")) {
                    UnitPrice = getResources().getStringArray(R.array.unitPriceCash);
                } else {
                    UnitPrice = getResources().getStringArray(R.array.unitPriceDue);
                }
                if (itemNames.size() == 0) {
                    addItem();
                } else {
                    if (itemNames.contains(itemName)) {
                        Toast.makeText(getApplicationContext(), "This item is exists!", Toast.LENGTH_LONG).show();
                    } else {
                        addItem();
                    }
                }

            }
        });

        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recyclerView = findViewById(R.id.recyclerView);
                adapter = new SalesOrderAdapter(items);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);

                if (adapter.getItemCount() > 0) {
                    openPlaceOrderDialog();
                } else {
                    Toast.makeText(getApplicationContext(), "Please add some items to proceed order!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getRecyclerViewItemPosition(int position) {
        SalesOrderItemList itemPosition = items.get(position);
        adapter.notifyItemChanged(position);
        Position = position;
    }

    public void populateRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(this);
        adapter = null;
        adapter = new SalesOrderAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new SalesOrderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                getRecyclerViewItemPosition(position);
                openRecyclerViewItemDeleteDialog();
            }
        });
    }

    public void Post() {
        for (t = 0; t < items.size(); t++) {
            soNo = t + 1;
            databaseHelper.insertSalesOrder(sellerId, String.valueOf(soNo), paymentType, distributorId, dateOfSale, dateOfDelivery, items.get(t).getItem(), String.valueOf(items.get(t).getQuantity()), String.valueOf(items.get(t).getPrice()), String.valueOf(items.get(t).getDiscount()));
            Call<SalesOrderResponse> call = new RetrofitClient()
                    .getApi()
                    .salesOrder(sellerId, String.valueOf(soNo), paymentType, distributorId, dateOfSale, dateOfDelivery, items.get(t).getItem(), String.valueOf(items.get(t).getQuantity()), String.valueOf(items.get(t).getPrice()), String.valueOf(items.get(t).getDiscount()));

            call.enqueue(new Callback<SalesOrderResponse>() {
                @Override
                public void onResponse(Call<SalesOrderResponse> call, Response<SalesOrderResponse> response) {
                    SalesOrderResponse addItemResponse = response.body();
                    //showNotification(getApplicationContext(), "Order status!", addItemResponse.getResponse());
                }

                @Override
                public void onFailure(Call<SalesOrderResponse> call, Throwable t) {
                    Toast.makeText(getApplication(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void sellerActivityUpdate() {
        Call<SellerActivityUpdateResponse> call = new RetrofitClient()
                .getApi()
                .sellerActivityUpdate(String.valueOf(grandTotal));

        call.enqueue(new Callback<SellerActivityUpdateResponse>() {
            @Override
            public void onResponse(Call<SellerActivityUpdateResponse> call, Response<SellerActivityUpdateResponse> response) {

            }

            @Override
            public void onFailure(Call<SellerActivityUpdateResponse> call, Throwable t) {
                Toast.makeText(getApplication(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addItem() {
        if (itemName.equals("") || itemCode.equals("") || quantityString.equals("")) {
            Toast.makeText(getApplicationContext(), "Do not leave any input blank", Toast.LENGTH_SHORT).show();
        } else {
            for (i = 0; i < Products.length; i++) {
                if (itemName.equals(Products[i])) {
                    price = Integer.parseInt(UnitPrice[i]);
                }
            }
            quantityInt = Integer.parseInt(quantityString);
            discountDouble = (price * percent) / 100;
            amount = (price - discountDouble) * quantityInt;
            discountString = percent + "%";

            itemNames.add(itemName);
            items.add(new SalesOrderItemList(itemName, quantityInt, price, discountString, amount));
            populateRecyclerView();

            totalAmount += amount;
            grandTotal = ((int) totalAmount);
            textVIewTotalAmount.setText(String.valueOf(grandTotal));

            editTextItemName.setText("");
            editTextItemCode.setText("");
            editTextQuantity.setText("");
        }
    }

    public void removeItem(int position) {
        subtractTotal = (int) items.get(position).getAmount();
        totalAmount -= subtractTotal;
        grandTotal = ((int) totalAmount);
        textVIewTotalAmount.setText(String.valueOf(grandTotal));

        items.remove(position);
        adapter.notifyItemRemoved(position);

        populateRecyclerView();

        Toast.makeText(SalesOrder.this, "Item removed!", Toast.LENGTH_SHORT).show();
    }

    public void openPlaceOrderDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Place Order");
        adb.setMessage("Do you want to place order?");
        adb.setIcon(R.drawable.ic_confirm);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (connectionChecker.isConnected()) {
                    Post();
                    sellerActivityUpdate();
                    Intent i = new Intent(SalesOrder.this, SalesInvoice.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adb.show();
    }

    public void openRecyclerViewItemDeleteDialog() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Remove Item");
        adb.setMessage("Do you want to remove this item?");
        adb.setIcon(R.drawable.ic_remove_item);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                removeItem(Position);
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adb.show();
    }

//    public void showNotification(Context context, String title, String body) {
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        int notificationId = 1;
//        String channelId = "channel-01";
//        String channelName = "GME AGRO";
//        int importance = NotificationManager.IMPORTANCE_HIGH;
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            NotificationChannel mChannel = new NotificationChannel(
//                    channelId, channelName, importance);
//            notificationManager.createNotificationChannel(mChannel);
//        }
//
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
//                .setSmallIcon(R.drawable.logo_gme)
//                .setContentTitle(title)
//                .setContentText(body);
//
//        notificationManager.notify(notificationId, mBuilder.build());
//    }

    @Override
    public void onBackPressed() {
        if (backPressTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();

        } else {
            backToast = Toast.makeText(getApplicationContext(), "Press back again to go back", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressTime = System.currentTimeMillis();
    }
}
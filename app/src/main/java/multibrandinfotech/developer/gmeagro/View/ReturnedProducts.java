package multibrandinfotech.developer.gmeagro.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.ProductReturnItemList;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.Model.ReturnedProductResponse;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.ProductReturnAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReturnedProducts extends AppCompatActivity {

    private InternetConnectionChecker connectionChecker;
    private String distributorId, sellerId;
    private String[] itemNames, quantities, invoiceNumbers, returnDates;

    private RecyclerView recyclerView;
    private ProductReturnAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<ProductReturnItemList> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_returned_product_list);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Returned Products");

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("MySharedPreference", MODE_PRIVATE);
        sellerId = sharedPref.getString("user", "");

        connectionChecker = new InternetConnectionChecker(getApplicationContext());
        distributorId = getIntent().getStringExtra("distributor");

        recyclerView = findViewById(R.id.recyclerView_returnedProducts);

        if (connectionChecker.isConnected()) {
            getProductList();
        } else {
            Toast.makeText(getApplicationContext(), "Internet connection required!", Toast.LENGTH_SHORT).show();
        }
    }

    public void populateRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getApplicationContext());
        adapter = null;
        adapter = new ProductReturnAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void getProductList() {
        Call<List<ReturnedProductResponse>> call = RetrofitClient
                .getInstance()
                .getApi().getReturnedProducts(distributorId);

        call.enqueue(new Callback<List<ReturnedProductResponse>>() {
            @Override
            public void onResponse(Call<List<ReturnedProductResponse>> call, Response<List<ReturnedProductResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Response unsuccessful! " + response.message(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<ReturnedProductResponse> responses = response.body();
                itemNames = new String[responses.size()];
                quantities = new String[responses.size()];
                invoiceNumbers = new String[responses.size()];
                returnDates = new String[responses.size()];

                for (int i = 0; i < responses.size(); i++) {
                    itemNames[i] = responses.get(i).getItemName();
                    quantities[i] = responses.get(i).getQuantity();
                    invoiceNumbers[i] = responses.get(i).getInvoiceNo();
                    returnDates[i] = responses.get(i).getReturnDate();

                    items.add(new ProductReturnItemList(itemNames[i], quantities[i], invoiceNumbers[i]));
                    populateRecyclerView();
                }
            }

            @Override
            public void onFailure(Call<List<ReturnedProductResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error!!! " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Error!!!", t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reports, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDashboard:
                Intent i = new Intent(getApplicationContext(), Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuSellItems:
                Intent i1 = new Intent(getApplicationContext(), IndentForm.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuPaymentCollection:
                Intent i2 = new Intent(getApplicationContext(), PaymentCollection.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;

            case R.id.menuProductReturn:
                Intent i3 = new Intent(getApplicationContext(), ProductReturn.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i3.putExtra("user", sellerId);
                startActivity(i3);
                break;

            case R.id.menuSpecialOffers:
                Intent i4 = new Intent(getApplicationContext(), SpecialOffers.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i4.putExtra("user", sellerId);
                startActivity(i4);
                break;

            case R.id.menuSignOut:
                Intent i5 = new Intent(getApplicationContext(), Login.class);
                i5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i5);
                break;

            case R.id.menuAbout:
                Intent i6 = new Intent(getApplicationContext(), About.class);
                i6.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i6);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Reports.class);
        intent.putExtra("user", sellerId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
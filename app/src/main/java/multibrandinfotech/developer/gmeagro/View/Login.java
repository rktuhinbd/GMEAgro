package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import multibrandinfotech.developer.gmeagro.Model.LoginResponse;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    private InternetConnectionChecker connectionChecker;
    private long backPressTime;
    private Toast backToast;
    private TextInputLayout editTextUserName, editTextPassword;
    private Button buttonLogin;
    private String userNameText, passwordText;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        connectionChecker = new InternetConnectionChecker(this);
        editTextUserName = findViewById(R.id.editText_UserName);
        editTextPassword = findViewById(R.id.editText_Password);
        buttonLogin = (Button) findViewById(R.id.button_Login);

        //Set login button Action
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userNameText = editTextUserName.getEditText().getText().toString();
                passwordText = editTextPassword.getEditText().getText().toString();

                if (connectionChecker.isConnected()) {
                    //Check if any of input is blank, if blank set error message
                    if (userNameText.isEmpty() || passwordText.isEmpty()) {
                        userNameInputChecker();
                        passwordInputChecker();
                    } else {
                        //Parse data from online database
                        checkJSONData();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void userNameInputChecker() {
        if (userNameText.isEmpty()) {
            editTextUserName.setError("User name must be entered");
        } else {
            editTextUserName.setError(null);
        }
    }

    public void passwordInputChecker() {
        if (passwordText.isEmpty()) {
            editTextPassword.setError("Password must be entered");
        } else {
            editTextPassword.setError(null);
        }
    }

    public void checkJSONData() {
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().loginData(userNameText, passwordText);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                if (response.isSuccessful()) {
                    //Check if both input and online data matches
                    if (loginResponse.getStat().equals("1")) {
                        editTextUserName.setError(null);
                        editTextPassword.setError(null);
                        Intent intent = new Intent(getApplicationContext(), Home.class);
                        intent.putExtra("user", userNameText);
                        startActivity(intent);
                    } else {
                        editTextUserName.setError("Wrong User name or Password");
                        editTextPassword.setError("Wrong User name or Password");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("Login Exception: ", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (backPressTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        } else {
            backToast = Toast.makeText(getApplicationContext(), "Press back again to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressTime = System.currentTimeMillis();
    }
}
package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.PaymentCollectionHistoryItemList;
import multibrandinfotech.developer.gmeagro.Model.PaymentCollectionHistoryResponse;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.PaymentCollectionHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class PaymentCollectionFragment extends Fragment {

    private InternetConnectionChecker connectionChecker;
    private String[] distributors, amounts, moneyReceiptNumbers, dates;
    private String sellerId;

    private RecyclerView recyclerView;
    private PaymentCollectionHistoryAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<PaymentCollectionHistoryItemList> items = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        connectionChecker = new InternetConnectionChecker(getActivity());
        View view = inflater.inflate(R.layout.fragment_payment_collection, container, false);

        SharedPreferences sharedPref = getContext().getSharedPreferences("MySharedPreference", MODE_PRIVATE);
        sellerId = sharedPref.getString("user", "");
        recyclerView = view.findViewById(R.id.recyclerView_paymentCollectionHistory);

        if (connectionChecker.isConnected()) {
            Call<List<PaymentCollectionHistoryResponse>> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .getPaymentCollectionHistory(sellerId);

            call.enqueue(new Callback<List<PaymentCollectionHistoryResponse>>() {
                @Override
                public void onResponse(Call<List<PaymentCollectionHistoryResponse>> call, Response<List<PaymentCollectionHistoryResponse>> response) {
                    if (!response.isSuccessful()) {
                        Toast.makeText(getContext(), "Response unsuccessful!!!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    List<PaymentCollectionHistoryResponse> responses = response.body();
                    distributors = new String[responses.size()];
                    amounts = new String[responses.size()];
                    moneyReceiptNumbers = new String[responses.size()];
                    dates = new String[responses.size()];

                    for (int i = 0; i < responses.size(); i++) {
                        distributors[i] = responses.get(i).getDistributorId();
                        amounts[i] = responses.get(i).getAmount();
                        moneyReceiptNumbers[i] = responses.get(i).getMoneyReceiptNo();
                        dates[i] = responses.get(i).getReceiveDate();

                        items.add(new PaymentCollectionHistoryItemList("৳" + amounts[i], "MR# " + moneyReceiptNumbers[i], dates[i], distributors[i]));
                        populateRecyclerView();
                    }
                }

                @Override
                public void onFailure(Call<List<PaymentCollectionHistoryResponse>> call, Throwable t) {
                    Toast.makeText(getContext(), "Error!!! " + t.getMessage(), Toast.LENGTH_LONG).show();
                    Log.e("Error!!!", t.getMessage());
                }
            });
        } else {
            Toast.makeText(getActivity(), "Check your internet connection!", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    public void populateRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getContext());
        adapter = null;
        adapter = new PaymentCollectionHistoryAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.DatePickerFragment;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;

public class IndentForm extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static String[] distributorArray, partyCodeArray;
    private static ArrayAdapter<String> cityAdapter, countryAdapter;
    private InternetConnectionChecker connectionChecker;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private EditText editTextDatePicker;
    private AutoCompleteTextView editTextDistributor, editTextPartyCode;
    private Button buttonProceed;
    private String paymentType, orderDate, deliveryDate, distributor, partycode, flagCash = "1", sellerId;
    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            for (int i = 0; i < distributorArray.length; i++) {
                if (s.toString().equals(distributorArray[i])) {
                    //Set Text in itemCode - AutoComplete Text Watcher
                    editTextPartyCode.setText(partyCodeArray[i]);
                }
            }
            editTextPartyCode.setEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent_form);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sales Order Form");

        sellerId = getIntent().getStringExtra("user");

        connectionChecker = new InternetConnectionChecker(this);

        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        editTextDatePicker = (EditText) findViewById(R.id.editText_DatePicker);
        editTextDistributor = (AutoCompleteTextView) findViewById(R.id.editText_Distributor);
        editTextPartyCode = (AutoCompleteTextView) findViewById(R.id.editText_Party_Code);
        buttonProceed = (Button) findViewById(R.id.button_Proceed);

        distributorArray = getResources().getStringArray(R.array.distributorName);
        partyCodeArray = getResources().getStringArray(R.array.partyCode);

        editTextDistributor.addTextChangedListener(watcher);
        editTextPartyCode.addTextChangedListener(watcher);

        //Setting Autocomplete TextField Array
        countryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, distributorArray);
        cityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, partyCodeArray);

        editTextDistributor.setAdapter(countryAdapter);
        editTextPartyCode.setAdapter(cityAdapter);

        editTextDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        buttonProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getApplicationContext(), "Please fill-up all details", Toast.LENGTH_LONG).show();
                } else {
                    paymentType = (String) radioButton.getText();
                    deliveryDate = editTextDatePicker.getText().toString();
                    distributor = editTextDistributor.getText().toString();
                    partycode = editTextPartyCode.getText().toString();

                    if (paymentType.equals(null) || deliveryDate.equals("") || distributor.equals("") || partycode.equals("")) {
                        Toast.makeText(getApplicationContext(), "Please fill-up all details", Toast.LENGTH_LONG).show();
                    } else {

                        if (connectionChecker.isConnected()) {

                        } else {
                            Toast.makeText(getApplicationContext(), "Internet connection not available!", Toast.LENGTH_SHORT).show();
                        }

                        if (!paymentType.equals("Cash")) {
                            flagCash = "0";
                        }

                        Intent i = new Intent(IndentForm.this, SalesOrder.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("flag", flagCash);
                        i.putExtra("seller_id", sellerId);
                        i.putExtra("payment_type", paymentType);
                        i.putExtra("distributor_id", distributor);
                        i.putExtra("date_of_sale", orderDate);
                        i.putExtra("date_of_delivery", deliveryDate);
                        startActivity(i);
                    }
                }
            }
        });
    }

    public void radioCheck(View view) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        orderDate = new SimpleDateFormat("yyyy-MM-dd").format(date.getTime());
        deliveryDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        editTextDatePicker.setText(deliveryDate);
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sales_order_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDashboard:
                Intent intent = new Intent(getApplicationContext(), Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("user", sellerId);
                startActivity(intent);
                break;

            case R.id.menuPaymentCollection:
                Intent i = new Intent(getApplicationContext(), PaymentCollection.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuProductReturn:
                Intent i1 = new Intent(getApplicationContext(), ProductReturn.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuSpeicalOffers:
                Intent i2 = new Intent(getApplicationContext(), SpecialOffers.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;

            case R.id.menuReports:
                Intent iR = new Intent(getApplicationContext(), Reports.class);
                iR.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iR.putExtra("user", sellerId);
                startActivity(iR);
                break;

            case R.id.menuSignOut:
                Intent i3 = new Intent(getApplicationContext(), Login.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.menuAbout:
                Intent i4 = new Intent(getApplicationContext(), About.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("user", sellerId);
        startActivity(intent);
    }
}
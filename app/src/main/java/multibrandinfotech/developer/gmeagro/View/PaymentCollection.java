package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import multibrandinfotech.developer.gmeagro.Model.DatabaseHelper;
import multibrandinfotech.developer.gmeagro.Model.PaymentCollectionResponse;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.cDatePickerFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentCollection extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static DatabaseHelper databaseHelper;
    private static String[] distributorArray, bankList;
    private InternetConnectionChecker connectionChecker;
    private AutoCompleteTextView editTextDistributor, editTextBankName;
    private EditText editTextMoneyReceiptNo, editTextAmount, editTextAccountName, editTextChequeNo, editTextChequeDate;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button buttonPay;
    private int distributorFlag = 0, moneyReceiptFlag = 0, amountFlag = 0, bankNameFlag = 0, accountNameFlag = 0, chequeNoFlag = 0, chequeDateFlag = 0;

    private String sellerId, paymentType, distributor, amount, moneyReceiptNo, bankName, accountName, chequeNo, chequeDate, currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_collection);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Payment Collection");

        sellerId = getIntent().getStringExtra("user");

        databaseHelper = new DatabaseHelper(this);
        connectionChecker = new InternetConnectionChecker(this);
        editTextDistributor = (AutoCompleteTextView) findViewById(R.id.editText_Distributor);
        editTextBankName = (AutoCompleteTextView) findViewById(R.id.editText_BankName);
        editTextMoneyReceiptNo = (EditText) findViewById(R.id.editText_MoneyReceiptNo);
        editTextAmount = (EditText) findViewById(R.id.editText_PaymentAmount);
        editTextAccountName = (EditText) findViewById(R.id.editText_AccountName);
        editTextChequeNo = (EditText) findViewById(R.id.editText_ChequeNo);
        editTextChequeDate = (EditText) findViewById(R.id.editText_ChequeDate);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        buttonPay = (Button) findViewById(R.id.button_Pay);

        distributorArray = getResources().getStringArray(R.array.distributorName);
        bankList = getResources().getStringArray(R.array.bankList);

        final ArrayAdapter<String> distributorAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, distributorArray);
        final ArrayAdapter<String> bankListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, bankList);

        editTextDistributor.setAdapter(distributorAdapter);
        editTextBankName.setAdapter(bankListAdapter);

        editTextChequeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new cDatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        buttonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distributorInputValidation();
                moneyReceiptInputValidation();
                amountInputValidation();
                bankNameInputValidation();
                accountNameInputValidation();
                chequeNoInputValidation();
                chequeDateInputValidation();

                int radioId = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(radioId);

                if (radioGroup.getCheckedRadioButtonId() == -1) {
                } else {
                    paymentType = (String) radioButton.getText();
                    if (paymentType.equals("Cheque")) {
                        if (distributorFlag == 1 && amountFlag == 1 && moneyReceiptFlag == 1 && bankNameFlag == 1 && accountNameFlag == 1 && chequeNoFlag == 1 && chequeDateFlag == 1) {
                            openPaymentCollectionDialog();
                        }
                    } else {
                        if (distributorFlag == 1 && amountFlag == 1 && moneyReceiptFlag == 1) {
                            openPaymentCollectionDialog();
                        }
                    }
                }
            }
        });
    }

    public void clearInputs() {
        radioGroup.clearCheck();
        editTextDistributor.setText("");
        editTextAmount.setText("");
        editTextMoneyReceiptNo.setText("");
        editTextBankName.setText("");
        editTextAccountName.setText("");
        editTextChequeNo.setText("");
        editTextChequeDate.setText("");
    }

    public void removeErrors() {
        editTextDistributor.setError(null);
        editTextAmount.setError(null);
        editTextMoneyReceiptNo.setError(null);
        editTextBankName.setError(null);
        editTextAccountName.setError(null);
        editTextChequeNo.setError(null);
        editTextChequeDate.setError(null);
    }

    public void radioCheck(View view) {
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);

        if (radioGroup.getCheckedRadioButtonId() == -1) {
        } else {
            paymentType = (String) radioButton.getText();
            if (paymentType.equals("Cheque")) {
                editTextBankName.setVisibility(View.VISIBLE);
                editTextAccountName.setVisibility(View.VISIBLE);
                editTextChequeNo.setVisibility(View.VISIBLE);
                editTextChequeDate.setVisibility(View.VISIBLE);
            } else {
                editTextBankName.setVisibility(View.GONE);
                editTextAccountName.setVisibility(View.GONE);
                editTextChequeNo.setVisibility(View.GONE);
                editTextChequeDate.setVisibility(View.GONE);
            }
        }
    }

    public void distributorInputValidation() {
        if (editTextDistributor.getText().toString().equals("")) {
            editTextDistributor.setError("Distributor is required!");
        } else {
            editTextDistributor.setError(null);
            distributorFlag = 1;
        }
    }

    public void moneyReceiptInputValidation() {
        if (editTextMoneyReceiptNo.getText().toString().equals("")) {
            editTextMoneyReceiptNo.setError("Money receipt is required!");
        } else {
            editTextMoneyReceiptNo.setError(null);
            moneyReceiptFlag = 1;
        }
    }

    public void amountInputValidation() {
        if (editTextAmount.getText().toString().equals("")) {
            editTextAmount.setError("Amount is required!");
        } else {
            editTextAmount.setError(null);
            amountFlag = 1;
        }
    }

    public void bankNameInputValidation() {
        if (editTextBankName.getText().toString().equals("")) {
            editTextBankName.setError("Bank name is required!");
        } else {
            editTextBankName.setError(null);
            bankNameFlag = 1;
        }
    }

    public void accountNameInputValidation() {
        if (editTextAccountName.getText().toString().equals("")) {
            editTextAccountName.setError("Account name is required!");
        } else {
            editTextAccountName.setError(null);
            accountNameFlag = 1;
        }
    }

    public void chequeNoInputValidation() {
        if (editTextChequeNo.getText().toString().equals("")) {
            editTextChequeNo.setError("Cheque number is required!");
        } else {
            editTextChequeNo.setError(null);
            chequeNoFlag = 1;
        }
    }

    public void chequeDateInputValidation() {
        if (editTextChequeDate.getText().toString().equals("")) {
            editTextChequeDate.setError("Cheque date is required!");
        } else {
            editTextChequeDate.setError(null);
            chequeDateFlag = 1;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        chequeDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        editTextChequeDate.setText(chequeDate);
    }

    public void postJSONData() {
        Call<PaymentCollectionResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .paymentCollection(paymentType, sellerId, distributor, amount, moneyReceiptNo, bankName, accountName, chequeNo, chequeDate, currentDate);

        call.enqueue(new Callback<PaymentCollectionResponse>() {
            @Override
            public void onResponse(Call<PaymentCollectionResponse> call, Response<PaymentCollectionResponse> response) {
                PaymentCollectionResponse paymentCollectionResponse = response.body();
                Toast.makeText(getApplicationContext(), paymentCollectionResponse.getResponse(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PaymentCollectionResponse> call, Throwable t) {
                Toast.makeText(getApplication(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void buttonAction() {
        //Get current date
        Date date = new Date();
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date.getTime());

        if (radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getApplicationContext(), "Please choose Payment type first", Toast.LENGTH_SHORT).show();

        } else {
            if (paymentType.equals("Cash")) {
                if (distributorFlag == 1 && moneyReceiptFlag == 1 && amountFlag == 1) {
                    distributorFlag = 0;
                    moneyReceiptFlag = 0;
                    amountFlag = 0;

                    distributor = editTextDistributor.getText().toString();
                    moneyReceiptNo = editTextMoneyReceiptNo.getText().toString();
                    amount = editTextAmount.getText().toString();
                    bankName = "";
                    accountName = "";
                    chequeNo = "";
                    chequeDate = "";

                    if (connectionChecker.isConnected()) {
                        postJSONData();
                        databaseHelper.insertPaymentReturn(paymentType, sellerId, distributor, amount, moneyReceiptNo, bankName, accountName, chequeNo, chequeDate, currentDate);
                        clearInputs();
                        removeErrors();
                    } else {
                        Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                if (distributorFlag == 1 && moneyReceiptFlag == 1 && amountFlag == 1 && bankNameFlag == 1 && accountNameFlag == 1 && chequeNoFlag == 1 && chequeDateFlag == 1) {
                    distributorFlag = 0;
                    moneyReceiptFlag = 0;
                    amountFlag = 0;
                    bankNameFlag = 0;
                    accountNameFlag = 0;
                    chequeNoFlag = 0;
                    chequeDateFlag = 0;

                    distributor = editTextDistributor.getText().toString();
                    moneyReceiptNo = editTextMoneyReceiptNo.getText().toString();
                    amount = editTextAmount.getText().toString();
                    bankName = editTextBankName.getText().toString();
                    accountName = editTextAccountName.getText().toString();
                    chequeNo = editTextChequeNo.getText().toString();
                    chequeDate = editTextChequeDate.getText().toString();

                    if (connectionChecker.isConnected()) {
                        postJSONData();
                        databaseHelper.insertPaymentReturn(paymentType, sellerId, distributor, amount, moneyReceiptNo, bankName, accountName, chequeNo, chequeDate, currentDate);
                        clearInputs();
                        removeErrors();
                    } else {
                        Toast.makeText(getApplicationContext(), "Internet Connection Required!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public void openPaymentCollectionDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Payment Collection");
        adb.setMessage("Do you want to submit Payment Collection request?");
        adb.setIcon(R.drawable.ic_confirm);

        adb.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                buttonAction();
            }
        });

        adb.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        adb.show();
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_payment_collection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDashboard:
                Intent i = new Intent(getApplicationContext(), Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuSellItems:
                Intent intent = new Intent(getApplicationContext(), IndentForm.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("user", sellerId);
                startActivity(intent);
                break;

            case R.id.menuProductReturn:
                Intent i1 = new Intent(getApplicationContext(), ProductReturn.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuSpeicalOffers:
                Intent i2 = new Intent(getApplicationContext(), SpecialOffers.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;

            case R.id.menuReports:
                Intent iR = new Intent(getApplicationContext(), Reports.class);
                iR.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                iR.putExtra("user", sellerId);
                startActivity(iR);
                break;

            case R.id.menuSignOut:
                Intent i3 = new Intent(getApplicationContext(), Login.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i3);
                break;

            case R.id.menuAbout:
                Intent i4 = new Intent(getApplicationContext(), About.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i4);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("user", sellerId);
        startActivity(intent);
    }
}
package multibrandinfotech.developer.gmeagro.View;

/**
 * Created by Md. Rejaul Karim on 1/2/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import multibrandinfotech.developer.gmeagro.R;

public class About extends AppCompatActivity {

    private String sellerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        sellerId = getIntent().getStringExtra("user");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("user", sellerId);
        startActivity(intent);
    }
}
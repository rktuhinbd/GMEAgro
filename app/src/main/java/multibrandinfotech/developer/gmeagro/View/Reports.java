package multibrandinfotech.developer.gmeagro.View;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import multibrandinfotech.developer.gmeagro.R;

public class Reports extends AppCompatActivity {

    public String sellerId;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_sales:
                    SharedPreferences sharedPref = getApplication().getSharedPreferences("MySharedPreference", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("user", sellerId);  // Pick a key here.
                    editor.commit();

                    selectedFragment = new SalesOrdersFragment();
                    break;
                case R.id.navigation_product_return:
                    SharedPreferences sharedPref1 = getApplication().getSharedPreferences("MySharedPreference", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor1 = sharedPref1.edit();
                    editor1.putString("user", sellerId);  // Pick a key here.
                    editor1.commit();

                    selectedFragment = new ProductReturnFragment();
                    break;
                case R.id.navigation_payment_collection:
                    SharedPreferences sharedPref2 = getApplication().getSharedPreferences("MySharedPreference", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor2 = sharedPref2.edit();
                    editor2.putString("user", sellerId);  // Pick a key here.
                    editor2.commit();

                    selectedFragment = new PaymentCollectionFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reports");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SalesOrdersFragment()).commit();

        sellerId = getIntent().getStringExtra("user");
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reports, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDashboard:
                Intent i = new Intent(getApplicationContext(), Home.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("user", sellerId);
                startActivity(i);
                break;

            case R.id.menuSellItems:
                Intent i1 = new Intent(getApplicationContext(), IndentForm.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("user", sellerId);
                startActivity(i1);
                break;

            case R.id.menuPaymentCollection:
                Intent i2 = new Intent(getApplicationContext(), PaymentCollection.class);
                i2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i2.putExtra("user", sellerId);
                startActivity(i2);
                break;


            case R.id.menuProductReturn:
                Intent i3 = new Intent(getApplicationContext(), ProductReturn.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i3.putExtra("user", sellerId);
                startActivity(i3);
                break;

            case R.id.menuSpecialOffers:
                Intent i4 = new Intent(getApplicationContext(), SpecialOffers.class);
                i4.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i4.putExtra("user", sellerId);
                startActivity(i4);
                break;

            case R.id.menuSignOut:
                Intent i5 = new Intent(getApplicationContext(), Login.class);
                i5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i5);
                break;

            case R.id.menuAbout:
                Intent i6 = new Intent(getApplicationContext(), About.class);
                i6.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i6);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.putExtra("user", sellerId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
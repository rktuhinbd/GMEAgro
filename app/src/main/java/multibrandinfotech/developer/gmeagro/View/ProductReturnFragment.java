package multibrandinfotech.developer.gmeagro.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.ProductReturnHistoryItemList;
import multibrandinfotech.developer.gmeagro.Model.ProductReturnHistoryResponse;
import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.ProductReturnHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class ProductReturnFragment extends Fragment {

    private InternetConnectionChecker connectionChecker;
    private String sellerId;
    private String[] distributors, returnDates;

    private RecyclerView recyclerView;
    private ProductReturnHistoryAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<ProductReturnHistoryItemList> items = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_return, container, false);

        connectionChecker = new InternetConnectionChecker(getContext());
        SharedPreferences sharedPref = getContext().getSharedPreferences("MySharedPreference", MODE_PRIVATE);
        sellerId = sharedPref.getString("user", "");
        recyclerView = view.findViewById(R.id.recyclerView);

        if (connectionChecker.isConnected()) {
            getProductReturnData();
        } else {
            Toast.makeText(getContext(), "Check your internet connection!", Toast.LENGTH_LONG).show();
        }
        return view;
    }

    public void getProductReturnData() {
        Call<List<ProductReturnHistoryResponse>> call = RetrofitClient
                .getInstance()
                .getApi()
                .getProductReturnHistory(sellerId);

        call.enqueue(new Callback<List<ProductReturnHistoryResponse>>() {
            @Override
            public void onResponse(Call<List<ProductReturnHistoryResponse>> call, Response<List<ProductReturnHistoryResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Response unsuccessful!", Toast.LENGTH_LONG).show();
                    return;
                }
                List<ProductReturnHistoryResponse> responses = response.body();
                distributors = new String[responses.size()];
                returnDates = new String[responses.size()];

                for (int i = 0; i < responses.size(); i++) {
                    distributors[i] = responses.get(i).getDistributor();
                    returnDates[i] = responses.get(i).getReturnDate();

                    items.add(new ProductReturnHistoryItemList(distributors[i], returnDates[i]));
                    populateRecyclerView();
                }
            }

            @Override
            public void onFailure(Call<List<ProductReturnHistoryResponse>> call, Throwable t) {
                Toast.makeText(getContext(), "Error!!! " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Error!!!", t.getMessage());
            }
        });
    }

    public void getRecyclerViewItemPosition(int position) {
        ProductReturnHistoryItemList itemPosition = items.get(position);
        adapter.notifyItemChanged(position);
    }

    public void populateRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getContext());
        adapter = null;
        adapter = new ProductReturnHistoryAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ProductReturnHistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                getRecyclerViewItemPosition(position);
                String distributor = items.get(position).getDistributor();

                Intent intent = new Intent(getContext(), ReturnedProducts.class);
                intent.putExtra("distributor", distributor);
                startActivity(intent);
            }
        });
    }
}
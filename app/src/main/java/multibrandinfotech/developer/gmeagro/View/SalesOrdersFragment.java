package multibrandinfotech.developer.gmeagro.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import multibrandinfotech.developer.gmeagro.Model.RetrofitClient;
import multibrandinfotech.developer.gmeagro.Model.SalesOrderHistoryResponse;
import multibrandinfotech.developer.gmeagro.R;
import multibrandinfotech.developer.gmeagro.ViewModel.InternetConnectionChecker;
import multibrandinfotech.developer.gmeagro.ViewModel.SalesOrderHistoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Md. Rejaul Karim on 1/26/2019.
 * Copyright (c) 2019 MULTIBRAND INFOTECH LTD
 */
public class SalesOrdersFragment extends Fragment {

    private InternetConnectionChecker connectionChecker;
    private String sellerId;
    private String[] distributorIds, dateOfSales;

    private RecyclerView recyclerView;
    private SalesOrderHistoryAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<SalesOrderHistoryResponse> items = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_orders, container, false);

        connectionChecker = new InternetConnectionChecker(getContext());
        SharedPreferences sharedPref = getContext().getSharedPreferences("MySharedPreference", MODE_PRIVATE);
        sellerId = sharedPref.getString("user", "");
        recyclerView = view.findViewById(R.id.recyclerView_salesOrders);

        if (connectionChecker.isConnected()) {
            getProductReturnData();
        } else {
            Toast.makeText(getContext(), "Check your internet connection!", Toast.LENGTH_LONG).show();
        }
        return view;
    }

    private void getProductReturnData() {
        Call<List<SalesOrderHistoryResponse>> call = RetrofitClient
                .getInstance()
                .getApi()
                .getSalesOrderHistory(sellerId);

        call.enqueue(new Callback<List<SalesOrderHistoryResponse>>() {
            @Override
            public void onResponse(Call<List<SalesOrderHistoryResponse>> call, Response<List<SalesOrderHistoryResponse>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Response unsuccessful!", Toast.LENGTH_LONG).show();
                    return;
                }
                List<SalesOrderHistoryResponse> responses = response.body();
                distributorIds = new String[responses.size()];
                dateOfSales = new String[responses.size()];

                for (int i = 0; i < responses.size(); i++) {
                    distributorIds[i] = responses.get(i).getDistributor();
                    dateOfSales[i] = responses.get(i).getDateOfSale();

                    items.add(new SalesOrderHistoryResponse(distributorIds[i], dateOfSales[i]));
                    populateRecyclerView();
                }
            }

            @Override
            public void onFailure(Call<List<SalesOrderHistoryResponse>> call, Throwable t) {
                Toast.makeText(getContext(), "Error!!! " + t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Error!!!", t.getMessage());
            }
        });
    }

    public void getRecyclerViewItemPosition(int position) {
        SalesOrderHistoryResponse itemPosition = items.get(position);
        adapter.notifyItemChanged(position);
    }

    public void populateRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getContext());
        adapter = null;
        adapter = new SalesOrderHistoryAdapter(items);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new SalesOrderHistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                getRecyclerViewItemPosition(position);
                String distributor = items.get(position).getDistributor();

                Intent intent = new Intent(getContext(), SalesOrderHistoryItems.class);
                intent.putExtra("distributor", distributor);
                startActivity(intent);
            }
        });
    }
}
